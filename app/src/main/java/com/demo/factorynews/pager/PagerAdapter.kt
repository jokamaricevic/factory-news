package com.demo.factorynews.pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.demo.factorynews.Article

class PagerAdapter(fragmentManager: FragmentManager, private val articles: ArrayList<Article>) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getCount(): Int {
        return articles.size
    }

    override fun getItem(position: Int): Fragment {
        return PageFragment(articles.get(position))

    }


}