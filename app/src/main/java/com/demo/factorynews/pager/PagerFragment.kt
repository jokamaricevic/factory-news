package com.demo.factorynews.pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.demo.factorynews.Article
import com.demo.factorynews.R
import kotlinx.android.synthetic.main.layout_pager_fragment.*
import kotlinx.android.synthetic.main.layout_pager_fragment.view.*

class PagerFragment(private val articles: ArrayList<Article>) : Fragment() {

    private lateinit var titleBar: TextView
    private lateinit var pagerAdapter: PagerAdapter
    private lateinit var viewPager: ViewPager
    private  lateinit var backButton: ImageView

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_pager_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pagerAdapter = PagerAdapter(childFragmentManager, articles)
        viewPager = view_pager
        viewPager.adapter = pagerAdapter
        viewPager.addOnPageChangeListener(getPageChangeListener())

        titleBar = view.pager_fragment_title

        backButton = view.pager_fragment_back
        backButton.setOnClickListener{
            fragmentManager?.beginTransaction()
                    ?.hide(this)
                    ?.commit()
        }
    }

    fun setCurrentItem(position: Int){
        viewPager.currentItem = position
    }

    private fun getPageChangeListener(): ViewPager.OnPageChangeListener {

        return object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int,
                                        positionOffset: Float,
                                        positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                titleBar.text = articles.get(position).title
            }

            override fun onPageScrollStateChanged(state: Int) {}

        }
    }
}