package com.demo.factorynews.pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.demo.factorynews.Article
import com.demo.factorynews.R
import kotlinx.android.synthetic.main.layout_page_fragment.view.*

open class PageFragment(private val article: Article) : Fragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.layout_page_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pageImage: ImageView = view.page_fragment_image
        val pageTitle: TextView = view.page_fragment_title
        val pageBody: TextView = view.page_fragment_body

        pageTitle.text = article.title
        pageBody.text = article.descritpion

        val requestOptions = RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_background)


        Glide.with(view.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(article.urlToImage)
                .into(pageImage)
    }
}