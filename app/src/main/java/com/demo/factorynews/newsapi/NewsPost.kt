package com.demo.factorynews.newsapi

data class NewsPost (
    var title: String,
    var body: String,
    var image: String )