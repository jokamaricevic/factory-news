package com.demo.factorynews.newsapi

import android.app.Application
import com.demo.factorynews.NewsResponse
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

class NewsServices {

    companion object{
        private const val CACHE_CONTROL_HEADER = "Cache-Control"
        private const val CACHE_SIZE = 10 * 1024 * 1024L
        private const val BASE_URL = "https://newsapi.org/"
        private const val SOURCE = "bbc-news"
        private const val SORTBY = "top"
        private const val APIKEY = "75702474c08c4c0c96c4081147233679"
    }

    fun getCall(application: Application): Call<NewsResponse> {
        val retrofit = retrofit(okHttp(httpCache(application )))
        val service = retrofit.create(NewsService::class.java)
        return service.getCurrentNews(SOURCE, SORTBY, APIKEY)
    }

    private fun retrofit(okHttpClient: OkHttpClient) = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

    private fun okHttp(cache: Cache): OkHttpClient {
        return OkHttpClient.Builder()
                .cache(cache)
                .addInterceptor(getLoggingInterceptor())
                .addNetworkInterceptor(CacheInterceptor())
                .build()
    }

    private fun httpCache(application: Application): Cache {
        val httpCacheDirectory = File(application.applicationContext.cacheDir, "http-cache")

        return Cache(httpCacheDirectory, CACHE_SIZE)
    }

    private fun getLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC)
        return loggingInterceptor
    }

    class CacheInterceptor : Interceptor {

        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            val response = chain.proceed(chain.request())

            val cacheControl = CacheControl.Builder()
                    .maxAge(5, TimeUnit.MINUTES)
                    .build()

            return response.newBuilder()
                    .removeHeader("Pragma")
                    .removeHeader("Cache-Control")
                    .header(CACHE_CONTROL_HEADER, cacheControl.toString())
                    .build()

        }

    }

}