package com.demo.factorynews.newsapi

import com.demo.factorynews.NewsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {
    @GET("v1/articles?")
    fun getCurrentNews(@Query("source") source: String,
                       @Query("sortBy") sortBy: String,
                       @Query("apiKey") apiKey: String): Call<NewsResponse>
}