 package com.demo.factorynews

import com.google.gson.annotations.SerializedName

class NewsResponse {

    @SerializedName("source")
    var source: String? = null
    @SerializedName("articles")
    var articles = ArrayList<Article>()
}

class Article {
    @SerializedName("title")
    var title: String? = null
    @SerializedName("description")
    var descritpion: String? = null

    @SerializedName("urlToImage")
    var urlToImage: String? = null

}

 
