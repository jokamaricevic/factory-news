package com.demo.factorynews

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.LinearInterpolator
import android.view.animation.RotateAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.LinearLayoutManager
import com.demo.factorynews.newsapi.NewsPost
import com.demo.factorynews.newsapi.NewsServices
import com.demo.factorynews.pager.PagerFragment
import com.demo.factorynews.recycler.PostRecyclerAdapter
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var postAdapter: PostRecyclerAdapter
    private lateinit var pagerFragment: PagerFragment
    private  lateinit var loadImage : ImageView
    private lateinit var loadText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)

        showSplashScreen()
        initRecyclerView()
        initRetrofit()
    }

    private fun showSplashScreen() {
        loadText = load_text
        loadImage = load_image
        val rotateAnimation = RotateAnimation(0F, 5 * 360F, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)
        rotateAnimation.duration = 5000
        rotateAnimation.interpolator = LinearInterpolator()
        loadImage.startAnimation(rotateAnimation)
    }

    private fun hideSplashScreen(){
        loadImage.clearAnimation()
        main_activity.removeView(loadImage)
        main_activity.removeView(loadText)
    }


    private fun addDataSet(articles: ArrayList<Article>){
        val postList: ArrayList<NewsPost> = ArrayList()
        articles.forEach{ postList.add(
            NewsPost(
            it.title.toString(),
            it.descritpion.toString(),
            it.urlToImage.toString()
        ))}

        postAdapter.addItems(postList)
        postAdapter.notifyDataSetChanged()
    }

    private fun initRecyclerView(){
        postAdapter = PostRecyclerAdapter()
        postAdapter.setOnItemClickListener(getOnItemListener())

        recycler_view.apply{
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = postAdapter
        }


    }

    private fun initRetrofit(){

        val call = NewsServices().getCall(application)

        call.enqueue(object : Callback<NewsResponse> {
            override fun onResponse(call: Call<NewsResponse>, response: Response<NewsResponse>) {
                if (response.isSuccessful){
                    val body = response.body()
                    addDataSet(body!!.articles)
                    pagerFragment = PagerFragment(body.articles)
                }
                if(response.code() == 429){
                    alert("Nedavno ste uputili previše zahtjeva.")
                }
                hideSplashScreen()
            }

            override fun onFailure(call: Call<NewsResponse>, t: Throwable) {
                alert("Ups, došlo je do pogreške.")
                hideSplashScreen()
            }
        } )
    }

    private fun alert(message: String) {
        val alertDialogBuilder = AlertDialog.Builder(this@MainActivity)
        alertDialogBuilder.setTitle("Greška")
        alertDialogBuilder.setMessage(message)
        alertDialogBuilder.setPositiveButton("U REDU"){ _, _ -> }
        alertDialogBuilder.setCancelable(false)
        alertDialogBuilder.show()

    }

    private fun getOnItemListener(): PostRecyclerAdapter.ClickListener {
        return object: PostRecyclerAdapter.ClickListener {
            override fun onItemClick(view: View, position: Int) {
                if(pagerFragment.isAdded){
                    pagerFragment.setCurrentItem(position)
                    supportFragmentManager.beginTransaction()
                            .show(pagerFragment)
                            .commitNow()

                } else {
                    supportFragmentManager.beginTransaction()
                            .add(R.id.main_activity , pagerFragment, "pagerFragment")
                            .commitNow()
                    pagerFragment.setCurrentItem(position)
                }
            }
        }
    }
}