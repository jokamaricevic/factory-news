package com.demo.factorynews.recycler

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.demo.factorynews.R
import com.demo.factorynews.newsapi.NewsPost
import kotlinx.android.synthetic.main.layout_news_list_item.view.*


class PostRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: List<NewsPost> = ArrayList()
    private var clickListener: ClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return PostViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.layout_news_list_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is PostViewHolder -> {
                holder.bind(items.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun addItems(postList: List<NewsPost>){
        items = postList

    }

    fun setOnItemClickListener(clickListener: ClickListener) {
        this.clickListener = clickListener
    }

    inner class PostViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener{

        private val postImage: ImageView = itemView.post_image
        private val postTitle: TextView = itemView.post_title

        init {
            if(clickListener != null){
                itemView.setOnClickListener(this)
            }
        }

        fun bind(newsPost: NewsPost){
            postTitle.text = newsPost.title

            val requestOptions = RequestOptions()
                    .placeholder(R.drawable.ic_launcher_background)
                    .error(R.drawable.ic_launcher_background)

            Glide.with(itemView.context)
                .applyDefaultRequestOptions(requestOptions)
                .load(newsPost.image)
                .centerCrop()
                .into(postImage)
        }

        override fun onClick(v: View?) {
            if (v != null){
                clickListener?.onItemClick(v, absoluteAdapterPosition)
            }
        }

    }

    interface ClickListener {
        fun onItemClick(view: View, position: Int)
    }


}